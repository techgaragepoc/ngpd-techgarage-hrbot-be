var builder = require('botbuilder');
var config = require('../config');
var common = require('../lib/common');

module.exports = [
    function (session, args, next) {
        var sendMessage = true;
        var documenttypes = builder.EntityRecognizer.findEntity(args.intent.entities, 'documenttypes');
        var career = builder.EntityRecognizer.findEntity(args.intent.entities, 'career');
        var checkin = builder.EntityRecognizer.findEntity(args.intent.entities, 'checkin');
        var role = builder.EntityRecognizer.findEntity(args.intent.entities, 'role');
        var community = builder.EntityRecognizer.findEntity(args.intent.entities, 'community');
        var onboarding = builder.EntityRecognizer.findEntity(args.intent.entities, 'onboarding');
        var peoplemanagement = builder.EntityRecognizer.findEntity(args.intent.entities, 'peoplemanagement');
        var start = builder.EntityRecognizer.findEntity(args.intent.entities, 'start');
        var performance = builder.EntityRecognizer.findEntity(args.intent.entities, 'performance');
        var human_resource = builder.EntityRecognizer.findEntity(args.intent.entities, 'human_resource');
        var time_of_the_day = builder.EntityRecognizer.findEntity(args.intent.entities, 'help');
        var still_there = builder.EntityRecognizer.findEntity(args.intent.entities, 'ask');
        var rating = builder.EntityRecognizer.findEntity(args.intent.entities, 'rating') && builder.EntityRecognizer.findEntity(args.intent.entities, 'ask');
        var information = builder.EntityRecognizer.findEntity(args.intent.entities, 'information');
        var competency = builder.EntityRecognizer.findEntity(args.intent.entities, 'competency');
        var communication = builder.EntityRecognizer.findEntity(args.intent.entities, 'communication');
        var help = builder.EntityRecognizer.findEntity(args.intent.entities, 'help');
        var new_hire = (help && help.resolution.values[0] == "newhire");
        var action = builder.EntityRecognizer.findEntity(args.intent.entities, 'action');
        var profile = builder.EntityRecognizer.findEntity(args.intent.entities, 'profile');
        var language = builder.EntityRecognizer.findEntity(args.intent.entities, 'language');

        var msg_out = common.getIntentNotFoundMessage(session);
        var msg_out_1 = "";


        if ((documenttypes && documenttypes.resolution.values[0] == "directors") || (role && role.entity == "mentor") || (role && role.entity == "manager" && communication)) {
            msg_out = common.getResponseWithGreetings(session) + config.get("dialog_information_director");
        }
        else if((language && language.resolution.values[0] == "workday") && ((action && action.resolution.values[0] == "access") || information)){
            msg_out = common.getResponseWithGreetings(session) + config.get("dialog_information_workday_access");
        }
        else if((language && language.resolution.values[0] == "workday") && documenttypes && documenttypes.resolution.values[0] == "goalsetting"){
            msg_out = common.getResponseWithGreetings(session) + config.get("dialog_information_workday_goalsetting") ;
        }
        else if ((language && language.resolution.values[0] == "workday") && profile && action && action.resolution.values[0] == "complete"){
            msg_out = common.getResponseWithGreetings(session) + config.get("dialog_information_workday_complete") ;
        }
        else if(language && language.resolution.values[0] == "workday"){
            msg_out = common.getResponseWithGreetings(session) + config.get("dialog_information_workday");
        }
        else if (role && competency && competency.entity == "criteria") {
            msg_out = common.getResponseWithGreetings(session) + config.get("dialog_information_competency_criteria");
        }
        else if (documenttypes && documenttypes.resolution.values[0] == "appraisal process") {
            msg_out = common.getResponseWithGreetings(session) + config.get("dialog_information_appraisal");
        }
        else if (documenttypes && documenttypes.resolution.values[0] == "goalsetting") {
            msg_out = common.getResponseWithGreetings(session) + config.get("dialog_information_goalsetting");
        }
        else if (career) {
            sendMessage = false;
            session.beginDialog('getCareerContext')
        }
        else if ((start && peoplemanagement) || ((onboarding || start) && role && role.resolution.values[0] == "manager")) {
            msg_out = common.getResponseWithGreetings(session) + config.get("dialog_information_onboarding_manager");
            msg_out_1 = config.get("dialog_information_onboarding_manager_1");
        }
        else if (onboarding) {
            msg_out = common.getResponseWithGreetings(session) + config.get("dialog_information_onboarding") ;
            msg_out_1 = config.get("dialog_information_onboarding_1");
        }
        else if ((documenttypes && documenttypes.resolution.values[0] == "3-2-1") || checkin || (performance && human_resource)) {
            msg_out = common.getResponseWithGreetings(session) + config.get("dialog_information_321");
        }
        else if (career && career.resolution.values[0] == "career story") {
            msg_out = common.getResponseWithGreetings(session) + config.get("dialog_expansion_career_story");
        }
        else if (new_hire) {
            msg_out = common.getResponseWithGreetings(session) + config.get("dialog_information_newhire");
        }
        /*else if ((start && peoplemanagement) || (start && role && role.resolution.values[0] == "manager")) {
            msg_out = "You can start by joining the People Manager Community where you can ask questions and access resources that will help you develop your people management skills. [People Manager Community](http://communities.mercer.com/peoplemanager/default.aspx)";
            msg_out_1 = "You can also visit the Managing People page on Me&My Career to access the People Manager Charter and other tools and resources that will help you understand your responsibilities as a people manager. [Managing People](http://sites.mercer.com/sites/HR/SitePages/ManagingPeople.aspx?nav=3h)";
        }*/
        else if (role && role.resolution.values[0] == "manager" || peoplemanagement) {
            if(peoplemanagement && peoplemanagement.entity == "pm"){
                sendMessage = false;
                session.beginDialog('getpmContext');
            }else{
                msg_out = common.getResponseWithGreetings(session) + config.get("dialog_pm_people");
            }
        }
        else if (time_of_the_day && time_of_the_day.resolution.values[0] === "time") {
            msg_out = config.get("dialog_information_time");
        }
        else if (still_there && still_there.entity === "there") {
            msg_out = 'Hey ' + session.message.user.name + '! How can I help you?';
        }
        else if (rating) {
            msg_out = config.get("dialog_information_rating");
        }
        else {
            //common.captureIntentMessage(session, args.intent, msg_out);
            //common.captureFeedback(session, { type: "No_MatchingResponse_Found", feedback: "No Matching Response found" });
            sendMessage = false;
            session.beginDialog('none');
        }
        
        
        if (sendMessage) {
            var card = JSON.parse(common.stringInject(JSON.stringify(config.get("standard_card_2")), [msg_out, msg_out_1]));
            common.captureIntentMessage(session, args.intent, msg_out + '  ' + msg_out_1);
            session.endDialog(new builder.Message(session).addAttachment({
                contentType: "application/vnd.microsoft.card.adaptive",
                content: card
            }));
        }
        sendMessage = true;
    }
]