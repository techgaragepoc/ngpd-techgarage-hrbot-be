var builder = require('botbuilder');
var config = require('../config');
var common = require('../lib/common');

module.exports = [

    function (session, args, next) {
        var sendMessage = true;
        var msg_out, msg_out_1 = "";
        var stretch = builder.EntityRecognizer.findEntity(args.intent.entities, 'stretch');
        var role = builder.EntityRecognizer.findEntity(args.intent.entities, 'role');
        var growth = builder.EntityRecognizer.findEntity(args.intent.entities, 'growth');
        var self = builder.EntityRecognizer.findEntity(args.intent.entities, 'self');
        var promotion = builder.EntityRecognizer.findEntity(args.intent.entities, 'promotion');
        var career = builder.EntityRecognizer.findEntity(args.intent.entities, 'career');
        var human_resource = builder.EntityRecognizer.findEntity(args.intent.entities, 'human_resource');

        msg_out = common.getIntentNotFoundMessage(session);

        if (stretch && human_resource) {
            msg_out = common.getResponseWithGreetings(session) + config.get("dialog_expansion_stretch_hr");
            msg_out_1 = config.get("dialog_expansion_stretch_hr_1");
        } else if (growth) {
            sendMessage = false;
            session.beginDialog('getGrowthContext');
        } else if (career) {
            sendMessage = false;
            session.beginDialog('getCareerContext');
        }
        else {
            //common.captureIntentMessage(session, args.intent, msg_out);
            //common.captureFeedback(session, { type: "No_MatchingResponse_Found", feedback: "No Matching Response found" });
            sendMessage = false;
            session.beginDialog('none');
        }
        
        if (sendMessage) {
            var card = JSON.parse(common.stringInject(JSON.stringify(config.get("standard_card_2")), [msg_out, msg_out_1]));

            session.endDialog(new builder.Message(session).addAttachment({
                contentType: "application/vnd.microsoft.card.adaptive",
                content: card
            }));
        }
        sendMessage = true;
    }
]