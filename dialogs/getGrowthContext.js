var builder = require('botbuilder');
var config = require('../config');
var common = require('../lib/common');
var msg_out = "", msg_out_1 = ""; 
module.exports = [
    function (session, next) {
        builder.Prompts.choice(session, "Are you asking about your Career Opportunities or Development Opportunities?", "Career Opportunities|Development Opportunities", { listStyle: builder.ListStyle.button });
    },
    function (session, results) {
        if (results.response) {
            if (results.response.entity == "Career Opportunities") {
                msg_out = common.getResponseWithGreetings(session) + config.get("dialog_expansion_growth_career");
            } else {
                msg_out = common.getResponseWithGreetings(session) + config.get("dialog_expansion_growth");
            }
            common.captureIntentMessage(session, { intent: results.response.entity, score: "1" }, msg_out + '  ' + msg_out_1);
            var card = JSON.parse(common.stringInject(JSON.stringify(config.get("standard_card_2")), [msg_out, msg_out_1]));
            session.endDialog(new builder.Message(session).addAttachment({
                contentType: "application/vnd.microsoft.card.adaptive",
                content: card
            }));

        }
    }
]