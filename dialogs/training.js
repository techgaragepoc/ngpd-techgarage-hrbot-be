var builder = require('botbuilder');
var config = require('../config');
var common = require('../lib/common');

//var msg_out = config.get('DEFAULT_MSG');
var msg_out = '';
var sendMessage = true;

var PMCardName = 'PM Card';
var CardNames = [PMCardName];
var isPmQuery = false;
var pmtype = '';

function createHeroCard(session, message, choices) {
    return new builder.HeroCard(session)
        .text(message)
        .buttons(choices.map(choice => new builder.CardAction.imBack(session, choice, choice)));
}

function createCard(session, selectedCardName, message, choices) {
    switch (selectedCardName) {
        case PMCardName:
            return createHeroCard(session, message, choices);
        default:
            return createHeroCard(session, message, choices);
    }
}

module.exports = [
    function (session, args, next) {
        var training_entities = args.intent.entities;

        msg_out = common.getIntentNotFoundMessage(session);
        msg_out_1 = "";

        //console.log('training entities=>' + JSON.stringify(training_entities));

        /* Training related queries */
        /* Start */
        var entity_lookup_role = builder.EntityRecognizer.findEntity(training_entities, 'role')

        var training_assertiveness = builder.EntityRecognizer.findEntity(training_entities, 'assertive');
        //training_assertiveness = training_assertiveness || (builder.EntityRecognizer.findEntity(training_entities, 'self') && builder.EntityRecognizer.findEntity(training_entities, 'advocacy'));

        //var training_self_learning_pm = builder.EntityRecognizer.findEntity(training_entities, 'learning');
        var training_self_learning_pm = (entity_lookup_role && entity_lookup_role.entity === 'pm');

        var training_projmgmt_learning = ((entity_lookup_role && entity_lookup_role.entity === 'project managers') || builder.EntityRecognizer.findEntity(training_entities, 'projectmanagement'));
        training_projmgmt_learning = training_projmgmt_learning && (builder.EntityRecognizer.findEntity(training_entities, 'training') || builder.EntityRecognizer.findEntity(training_entities, 'learning') ||
            builder.EntityRecognizer.findEntity(training_entities, 'information') || builder.EntityRecognizer.findEntity(training_entities, 'ask') ||
            builder.EntityRecognizer.findEntity(training_entities, 'about') || builder.EntityRecognizer.findEntity(training_entities, 'growth'));


        var training_projmgmt_learning = entity_lookup_role && entity_lookup_role.entity === 'project managers';

        training_projmgmt_learning = training_projmgmt_learning && (builder.EntityRecognizer.findEntity(training_entities, 'training') || builder.EntityRecognizer.findEntity(training_entities, 'learning'));
        training_projmgmt_learning = training_projmgmt_learning || builder.EntityRecognizer.findEntity(training_entities, 'information');

        //var training_pplmgmt_learning = entity_lookup_role && entity_lookup_role.entity === 'people managers';
        //training_pplmgmt_learning = training_pplmgmt_learning && (builder.EntityRecognizer.findEntity(training_entities, 'training') || builder.EntityRecognizer.findEntity(training_entities, 'learning'));
        //training_pplmgmt_learning = training_pplmgmt_learning || builder.EntityRecognizer.findEntity(training_entities, 'information');

        var training_pplmgmt_learning = ((entity_lookup_role && entity_lookup_role.entity === 'people managers') || builder.EntityRecognizer.findEntity(training_entities, 'peoplemanagement'));
        training_pplmgmt_learning = training_pplmgmt_learning && (builder.EntityRecognizer.findEntity(training_entities, 'training') || builder.EntityRecognizer.findEntity(training_entities, 'learning') ||
            builder.EntityRecognizer.findEntity(training_entities, 'information') || builder.EntityRecognizer.findEntity(training_entities, 'ask') ||
            builder.EntityRecognizer.findEntity(training_entities, 'about') || builder.EntityRecognizer.findEntity(training_entities, 'growth'));


        var training_presentation_communication = builder.EntityRecognizer.findEntity(training_entities, 'competency');
        //training_presentation_communication = training_presentation_communication || builder.EntityRecognizer.findEntity(training_entities, 'growth');
        training_presentation_communication = training_presentation_communication || builder.EntityRecognizer.findEntity(training_entities, 'communication');
        training_presentation_communication = training_presentation_communication || builder.EntityRecognizer.findEntity(training_entities, 'presentation');
        training_presentation_communication = training_presentation_communication || builder.EntityRecognizer.findEntity(training_entities, 'writing');
        training_presentation_communication = training_presentation_communication || builder.EntityRecognizer.findEntity(training_entities, 'facilitation');
        training_presentation_communication = training_presentation_communication || builder.EntityRecognizer.findEntity(training_entities, 'skills');

        var training_meeting = entity_lookup_role && (entity_lookup_role.entity === 'supervisor' || entity_lookup_role.entity === 'manager');
        training_meeting = training_meeting && (builder.EntityRecognizer.findEntity(training_entities, 'meeting') || builder.EntityRecognizer.findEntity(training_entities, 'advocacy'));
        // training_meeting = training_meeting || builder.EntityRecognizer.findEntity(training_entities, 'prepare');
        //training_meeting = training_meeting || builder.EntityRecognizer.findEntity(training_entities, 'self');
        // training_meeting = training_meeting || builder.EntityRecognizer.findEntity(training_entities, );
        //training_meeting = training_meeting || builder.EntityRecognizer.findEntity(training_entities, 'advocacy');
        //training_meeting = training_meeting || builder.EntityRecognizer.findEntity(training_entities, 'talk');

        var training_timemanagement = builder.EntityRecognizer.findEntity(training_entities, 'timemanagement');
        training_timemanagement = training_timemanagement || builder.EntityRecognizer.findEntity(training_entities, 'manage');
        training_timemanagement = training_timemanagement || builder.EntityRecognizer.findEntity(training_entities, 'time');
        //training_timemanagement = training_timemanagement || builder.EntityRecognizer.findEntity(training_entities, 'help');

        var training_feedback_difficult_conversation = builder.EntityRecognizer.findEntity(training_entities, 'communication');
        training_feedback_difficult_conversation = training_feedback_difficult_conversation || builder.EntityRecognizer.findEntity(training_entities, 'information');
        training_feedback_difficult_conversation = training_feedback_difficult_conversation && (builder.EntityRecognizer.findEntity(training_entities, 'difficult') || builder.EntityRecognizer.findEntity(training_entities, 'performance'));
        //training_feedback_difficult_conversation = training_feedback_difficult_conversation || builder.EntityRecognizer.findEntity(training_entities, 'handling');
        //training_feedback_difficult_conversation = training_feedback_difficult_conversation || builder.EntityRecognizer.findEntity(training_entities, 'advocacy');
        //training_feedback_difficult_conversation = training_feedback_difficult_conversation || builder.EntityRecognizer.findEntity(training_entities, 'performance');

        var training_leadership_development = builder.EntityRecognizer.findEntity(training_entities, 'leadership');
        training_leadership_development = training_leadership_development && (builder.EntityRecognizer.findEntity(training_entities, 'development') ||
            builder.EntityRecognizer.findEntity(training_entities, 'competency'));

        var training_dev_guide = entity_lookup_role && (entity_lookup_role.entity === 'supervisor' || entity_lookup_role.entity === 'managers');
        training_dev_guide = training_dev_guide && (builder.EntityRecognizer.findEntity(training_entities, 'help') || builder.EntityRecognizer.findEntity(training_entities, 'growth'));

        /* End */
        var projectmanagement = builder.EntityRecognizer.findEntity(args.intent.entities, 'projectmanagement');
        var develop = builder.EntityRecognizer.findEntity(args.intent.entities, 'develop');
        var learning = builder.EntityRecognizer.findEntity(args.intent.entities, 'learning');
        var presentation = builder.EntityRecognizer.findEntity(args.intent.entities, 'presentation');
        var writing = builder.EntityRecognizer.findEntity(args.intent.entities, 'writing');
        var communication = builder.EntityRecognizer.findEntity(args.intent.entities, 'communication');
        var role = builder.EntityRecognizer.findEntity(args.intent.entities, 'role');
        var advocacy = builder.EntityRecognizer.findEntity(args.intent.entities, 'advocacy');
        var training = builder.EntityRecognizer.findEntity(args.intent.entities, 'training');
        var timemanagement = builder.EntityRecognizer.findEntity(args.intent.entities, 'timemanagement');
        var self = builder.EntityRecognizer.findEntity(args.intent.entities, 'self');
        var community = builder.EntityRecognizer.findEntity(args.intent.entities, 'community');
        var leadership = builder.EntityRecognizer.findEntity(args.intent.entities, 'leadership');
        var performance = builder.EntityRecognizer.findEntity(args.intent.entities, 'performance');
        var human_resource = builder.EntityRecognizer.findEntity(args.intent.entities, 'human_resource');
        var manage = builder.EntityRecognizer.findEntity(args.intent.entities, 'manage');
        var growth = builder.EntityRecognizer.findEntity(args.intent.entities, 'growth');
        var negative = builder.EntityRecognizer.findEntity(args.intent.entities, 'negative');
        var difficult = builder.EntityRecognizer.findEntity(args.intent.entities, 'difficult');
        var documenttypes = builder.EntityRecognizer.findEntity(args.intent.entities, 'documenttypes');

        if (projectmanagement) {
            if (projectmanagement.entity === "pm") {
                isPmQuery = true;

                var message = 'Do you mean Project Management or People Manager?';

                var choices = ['Project Management', 'People Manager'];

                var pmCard = createCard(session, PMCardName, message, choices);

                const dialogMessage = new builder.Message(session).addAttachment(pmCard);

                //session.endConversation(dialogMessage);
                builder.Prompts.choice(session, dialogMessage, choices);
            }
            else {
                msg_out = "The Project Management Community has links to a toolkit and other resources you might find helpful. You can find those resources [here](http://communities.mercer.com/ProjectManagement/default.aspx)";
            }
        }
        else if (growth && manage) {
            msg_out = common.getResponseWithGreetings(session) + "Start by joining the [People Manager Community](http://communities.mercer.com/peoplemanager/default.aspx) where you can ask questions and access resources that will help you develop your people management skills.";
            msg_out_1 = "Then, visit the [Managing People page](http://sites.mercer.com/sites/HR/SitePages/ManagingPeople.aspx?nav=3h) on Me&My Career to access the People Manager Charter and other tools and resources that will help you understand your responsibilities as a people manager.";
        }
        else if (training && training.resolution.values[0] == "coach") {
            msg_out = common.getResponseWithGreetings(session) + config.get("dialog_training_coach");
        }
        else if (manage && manage.resolution.values[0] == "delegate") {
            msg_out = common.getResponseWithGreetings(session) + config.get("dialog_training_delegation");
        }
        else if (growth && writing && documenttypes && documenttypes.resolution.values[0] == "goalsetting") {
            msg_out = common.getResponseWithGreetings(session) + config.get("dialog_training_goalsetting");
        }
        else if ((develop || learning) && (presentation || communication || writing)) {
            msg_out = common.getResponseWithGreetings(session) + "You can find resources [here](http://sites.mercer.com/sites/HR/SitePages/influences-collaborates.aspx)";
        }
        else if (role && advocacy && training) {
            msg_out = common.getResponseWithGreetings(session) + config.get("dialog_training_advocacy");
        }
        else if ((role && training) || (training && develop && develop.resolution.values[0] == "recommend") || (training && self) || (training && community && community.resolution.values[0] == "department")) {
            msg_out = common.getResponseWithGreetings(session) + "That's a big question! Are you interested in business & financial acumen; communication; consulting; customer experience; leadership; people management; or sales? [Add LINKS]";
            msg_out_1 = "How much time do you have? [Add LINKS] Here is a summary of the face to face workshops that are available [here](http://sites.mercer.com/sites/HR/_layouts/DocIdRedir.aspx?ID=HRID-3-253). There are also a lot of other options available. Check out the development guides for each competency. You can get started [here](http://sites.mercer.com/sites/HR/SitePages/Capabilities.aspx).";
        }
        else if (timemanagement) {
            msg_out = common.getResponseWithGreetings(session) + "You can listen to [this](https://mercerlearning.webex.com/mercerlearning/lsr.php?RCID=5d056061b5524d7fbd8225e916ff71ec) excellent 30-minute recording of a live event";
            msg_out_1 = "Or you can use the Search feature in Careerlink to explore other resources. When you are in Careerlink, you might also want to check out the Harvard Resources by clicking on the link in the upper right green box on the home page.";
        }
        else if (training && leadership) {
            msg_out = common.getResponseWithGreetings(session) + "You have options to improve your Leadership behaviours. [Here](http://sites.mercer.com/sites/HR/_layouts/DocIdRedir.aspx?ID=HRID-3-355) is a toolkit that outlines development for leaders.";
        }
        else if (training_self_learning_pm) {
            var pm_clarification = builder.EntityRecognizer.findEntity(training_entities, 'projectmanagement');
            pm_clarification = pm_clarification || builder.EntityRecognizer.findEntity(training_entities, 'peoplemanagement');

            if (pm_clarification) {
                //The user has used the acronym PM
                isPmQuery = true;

                var message = 'Do you mean Project Management or People Manager?';

                var choices = ['Project Management', 'People Manager'];

                var pmCard = createCard(session, PMCardName, message, choices);

                const dialogMessage = new builder.Message(session).addAttachment(pmCard);

                //session.endConversation(dialogMessage);
                builder.Prompts.choice(session, dialogMessage, choices);
            }
        }

        else if (training_assertiveness) {
            //msg_out = "Expand the resources on self-advocacy that are on the following page. You can use these materials to have a local discussion. <b>Or</b> if you want to learn on your own print and read the <a href='http://communities.mercer.com/peoplemanager/Team%20Training/Forms/AllItems.aspx' target='_blank'>handouts</a>";
            msg_out = config.get("dialog_training_assertiveness");
        }
        else if (training_leadership_development) {
            //msg_out = "<a href='http://sites.mercer.com/sites/HR/_layouts/DocIdRedir.aspx?ID=HRID-3-355 target='_blank'>Here</a> is a toolkit that outlines development for leaders.";
            msg_out = '[Here](http://sites.mercer.com/sites/HR/_layouts/DocIdRedir.aspx?ID=HRID-3-355) is a toolkit that outlines development for leaders.';
        }
        else if (training_meeting) {
            //msg_out = "If you want to have a career development conversation, there are resources on the <a href='http://sites.mercer.com/sites/HR/SitePages/Careers.aspx' target='_blank'>Me and My Career page</a>, including the career development plan template. <a href='http://sites.mercer.com/sites/HR/Careers/Guide-to-Talking-with-Your-Manager.pdf' target='_blank'>This</a> guide to talking to your manager is also a good resource";
            msg_out = "If you want to have a career development conversation, there are resources on the [Me and My Career page](http://sites.mercer.com/sites/HR/SitePages/Careers.aspx), including the career development plan template. [This](http://sites.mercer.com/sites/HR/Careers/Guide-to-Talking-with-Your-Manager.pdf) guide to talking to your manager is also a good resource";
        }
        else if ((training_feedback_difficult_conversation) || (negative && information) || (negative && communication) || (difficult && communication)) {
            msg_out = "If you need help having a difficult conversation with someone who reports to you, speak with your [HR Partner](http://sites.mercer.com/sites/HR/HRContacts/contacts-list-global.aspx)";
            //msg_out = 'There are several online modules that address having difficult conversations.  Here are a few to try [Having a Difficult Conversation](https://mmc.csod.com/LMS/LoDetails/DetailsLo.aspx?loid=3abd65a0-613e-4f1a-8dae-a22343b4c46d&query=%3fq%3dHaving+a+Difficult+Conversation#t=1) **Or** [Preparing for a Difficult Conversation](https://mmc.csod.com/LMS/LoDetails/DetailsLo.aspx?loid=a1802766-730a-4e59-904a-72de834b08b2&query=%3fq%3dPreparing+for+a+Difficult+Conversation#t=1) **Or** [Handling Difficult Conversations Effectively](https://mmc.csod.com/LMS/LoDetails/DetailsLo.aspx?loid=cd328ef0-c196-4008-9d32-3396d48c7473&query=%3fq%3dHandling+Difficult+Conversations+Effectively#t=1) **Or** [Working with Difficult People](https://mmc.csod.com/LMS/LoDetails/DetailsLo.aspx?loid=8aa206aa-6c45-4f11-b8f6-126c65daee13&query=%3fq%3dWorking+with+Difficult+People%3a+Identifying+Difficult+People#t=1)';
            //msg_out = 'There are several online modules that address having difficult conversations. Here are a few to try - [Having a Difficult Conversation](' + decodeURIComponent('https://mmc.csod.com/LMS/LoDetails/DetailsLo.aspx?loid=3abd65a0-613e-4f1a-8dae-a22343b4c46d&query=%3fq%3dHaving+a+Difficult+Conversation#t=1') + ')\r- [Preparing for a Difficult Conversation](' + decodeURIComponent('https://mmc.csod.com/LMS/LoDetails/DetailsLo.aspx?loid=a1802766-730a-4e59-904a-72de834b08b2&query=%3fq%3dPreparing+for+a+Difficult+Conversation#t=1') + ')\r- [Handling Difficult Conversations Effectively](' + decodeURIComponent('https://mmc.csod.com/LMS/LoDetails/DetailsLo.aspx?loid=cd328ef0-c196-4008-9d32-3396d48c7473&query=%3fq%3dHandling+Difficult+Conversations+Effectively#t=1') + ')\r';
        }
        else if (training_projmgmt_learning) {
            msg_out = "The Project Management Community has links to a toolkit and other resources you might find helpful. You can find those resources [here](http://communities.mercer.com/ProjectManagement/default.aspx)";
        }
        else if (training_pplmgmt_learning) {
            msg_out = "The People Management Community has links to a toolkit and other resources you might find helpful. You can find those resources [here](http://communities.mercer.com/PeopleManagement/default.aspx)";
        }
        else if (training_presentation_communication) {
            //msg_out = "You can find resources <a href='http://sites.mercer.com/sites/HR/SitePages/influences-collaborates.aspx' target='_blank'>here</a>";
            msg_out = "You can find resources [here](http://sites.mercer.com/sites/HR/SitePages/influences-collaborates.aspx)";
        }
        else if (training_timemanagement) {
            //msg_out = "You can listen to <a href='https://mercerlearning.webex.com/mercerlearning/lsr.php?RCID=5d056061b5524d7fbd8225e916ff71ec' target='_blank'>this</a> excellent 30-minute recording of a live event <b>Or</b> you can use the Search feature in Careerlink to explore other resources. When you are in Careerlink, you might also want to check out the Harvard Resources by clicking on the link in the upper right green box on the home page.";
            msg_out = "You can listen to [this](https://mercerlearning.webex.com/mercerlearning/lsr.php?RCID=5d056061b5524d7fbd8225e916ff71ec) excellent 30-minute recording of a live event **Or** you can use the Search feature in Careerlink to explore other resources. When you are in Careerlink, you might also want to check out the Harvard Resources by clicking on the link in the upper right green box on the home page.";
        }
        else if (training_dev_guide) {
            //msg_out = "Development Guide for People Managers is available <a href='http://sites.mercer.com/sites/HR/SitePages/people-management-development.aspx' target='_blank'>here</a>";
            msg_out = 'Development Guide for People Managers is available [here](http://sites.mercer.com/sites/HR/SitePages/people-management-development.aspx)';
        }
        // Virtual Festival
        else if (training && (training.entity === 'virtual festival' || training.entity === 'virtualfestival' || training.entity === 'virtualfest' || training.entity === 'virtual fest')) {
            msg_out = "The Virtual Festival recordings (and schedules for upcoming Festivals) are [here](http://sites.mercer.com/sites/HR/SitePages/virtual-festival.aspx)";
            msg_out_1 = "Be sure to bookmark this page!";
        }
        //Workshop
        else if (training && training.entity === 'workshop') {
            msg_out = config.get("dialog_training_workshop");
        }
        else if (training) {
            msg_out = "Available training is listed on our Learning site: [Learning Page](http://sites.mercer.com/sites/HR/SitePages/Learning.aspx?nav=2h)";
        }
        else {
            //msg_out = "Sorry " + session.message.user.name + ", I haven’t learnt the answer to that question yet.  Right now I can help you with your development as a people manager, or the development of your direct reports.  I’m getting smarter every day though, so check back in a couple of weeks.";
            //common.captureIntentMessage(session, args.intent, msg_out);
            //common.captureFeedback(session, { type: "No_MatchingResponse_Found", feedback: "No Matching Response found" });
            sendMessage = false;
            session.beginDialog('none');
            
        }

        // console.log('entity_lookup_role: ' + JSON.stringify(entity_lookup_role));
        // console.log('training_entities: ' + JSON.stringify(training_entities));
        // console.log('training_assertiveness: ' + training_assertiveness);
        // console.log('training_dev_guide: ' + training_dev_guide);
        // console.log('training_feedback_difficult_conversation: ' + training_feedback_difficult_conversation);
        // console.log('training_leadership_development: ' + training_leadership_development);
        // console.log('training_meeting: ' + training_meeting);
        // console.log('training_pplmgmt_learning: ' + training_pplmgmt_learning);
        // console.log('training_presentation_communication: ' + training_presentation_communication);
        // console.log('training_projmgmt_learning: ' + training_projmgmt_learning);
        // console.log('training_self_learning_pm: ' + training_self_learning_pm);
        // console.log('training_timemanagement: ' + training_timemanagement);

        // console.log('msg_out: ' + msg_out);

        // Do not perform any action
        if (!isPmQuery) {

            // Defining Message template
            var card = JSON.parse(common.stringInject(JSON.stringify(config.get("standard_card_2")), [msg_out, msg_out_1]));

            if (sendMessage) {
                common.captureIntentMessage(session, args.intent, msg_out + ' ' + msg_out_1);
                session.endDialog(new builder.Message(session).addAttachment({
                    contentType: "application/vnd.microsoft.card.adaptive",
                    content: card
                }));
            }
            sendMessage = true;
        }
    },
    function (session, results) {
        //var msg_out = config.get('DEFAULT_MESSAGE');
        //console.log('response: ' + results.response.entity);

        if (results.response && results.response.entity && results.response.entity.toLowerCase() === 'project management') {
            pmtype = 'projmgmt';
        }
        else if (results.response && results.response.entity && results.response.entity.toLowerCase() === 'people manager') {
            pmtype = 'pplmgmt';
        }
        else {
            //session.endConversation(`Sorry, I didn't understand your choice.`);
            //session.endConversation(msg_out);
        }


        //console.log('pmtype: ' + pmtype);

        if (pmtype === 'projmgmt') {
            msg_out = 'The Project Management Community has links to a toolkit and other resources you might find helpful. You can find those resources [here](http://communities.mercer.com/ProjectManagement/default.aspx)';
            //<a href='http://communities.mercer.com/ProjectManagement/default.aspx' target='_blank'>Here</a>
        }
        else if (pmtype === 'pplmgmt') {
            //  msg_out = `The People Management Community has links to a toolkit and other resources you might find helpful. 
            //             You can find those resources <a href='http://communities.mercer.com/peoplemanager/default.aspx' target='_blank'>Here</a>`;            

            msg_out = 'The People Management Community has links to a toolkit and other resources you might find helpful. You can find those resources [here](http://communities.mercer.com/peoplemanager/default.aspx)';
        }

        //console.log('msg_out:' + msg_out);
        common.captureIntentMessage(session, { intent: "training", score: "1" }, msg_out + ' ' + msg_out_1);
        // Defining Message template
        var card = JSON.parse(common.stringInject(JSON.stringify(config.get("standard_card_2")), [msg_out, msg_out_1]));
        
        if (isPmQuery) {
            isPmQuery = false;
            session.endDialog(new builder.Message(session).addAttachment({
                contentType: "application/vnd.microsoft.card.adaptive",
                content: card
            }));
        }else{
            session.endDialog();
        }

    }
];