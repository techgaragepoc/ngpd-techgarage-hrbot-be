var builder = require('botbuilder');
var config = require('../config');
var common = require('../lib/common');

module.exports = [
    function (session, args, next) {
        var sendMessage = true;
        var msg_out, msg_out_1 = " ";
        var feedback = builder.EntityRecognizer.findEntity(args.intent.entities, 'feedback');
        var language = builder.EntityRecognizer.findEntity(args.intent.entities, 'language');
        var community = builder.EntityRecognizer.findEntity(args.intent.entities, 'community');
        msg_out = common.getIntentNotFoundMessage(session);

        if (language && language.resolution.values[0] == "workday" && feedback) {
            msg_out = common.getResponseWithGreetings(session) + config.get("dialog_feedback_workday");
        }
        else if (feedback) {
            msg_out = common.getResponseWithGreetings(session) + config.get("dialog_feedback");
            msg_out_1 = config.get("dialog_feedback_1");
        } else {
            //common.captureIntentMessage(session, args.intent, msg_out);
            //common.captureFeedback(session, { type: "No_MatchingResponse_Found", feedback: "No Matching Response found" });
            sendMessage = false;
            session.beginDialog('none');
        }

        
        var card = JSON.parse(common.stringInject(JSON.stringify(config.get("standard_card_2")), [msg_out, msg_out_1]));
        if (sendMessage) {
            common.captureIntentMessage(session, args.intent, msg_out + ' ' + msg_out_1);
            session.endDialog(new builder.Message(session).addAttachment({
                contentType: "application/vnd.microsoft.card.adaptive",
                content: card
            }));
        }
        sendMessage = true;
    }
]