var builder = require('botbuilder');
var config = require('../config');
var common = require('../lib/common');

module.exports = [

    function (session, args, next) {
        var personality_entities = args.intent.entities;

        var entity_lookup_help = builder.EntityRecognizer.findEntity(personality_entities, 'help');

        var msg_out = common.getIntentNotFoundMessage(session);
        var msg_out_1 = "";
        var sendMessage = true;

        if (builder.EntityRecognizer.findEntity(personality_entities, 'botlocation'))
            msg_out = "In this universe and in parallel universes";

        else if (entity_lookup_help && entity_lookup_help.resolution.values[0] == "stranger") {
            msg_out = "I know you " + session.message.user.name + ". Your identity has been authenticated already";
        } else if (entity_lookup_help && entity_lookup_help.resolution.values[0] == "surgery") {
            msg_out = "Please contact your HR Business Partner with any questions you have regarding this topic.";
        }
        else if (builder.EntityRecognizer.findEntity(personality_entities, 'name'))
            msg_out = common.getResponseWithGreetings(session) + "Good " + common.getDayTimeSlot(session.privateConversationData.intent_query.timestamp) + " My name is AIMEE. How can I help you?";

        else if (builder.EntityRecognizer.findEntity(personality_entities, 'botage'))
            msg_out = "Old enough to be answering your questions.";
        else if (builder.EntityRecognizer.findEntity(personality_entities, 'appearance'))
            msg_out = 'Happy and bright! How can i help you?';

        else if (builder.EntityRecognizer.findEntity(personality_entities, 'language'))
            msg_out = "At present I can understand and communicate in english only";

        else if (builder.EntityRecognizer.findEntity(personality_entities, 'communication'))
            msg_out = "At present I can understand and communicate in english only";

        else if (builder.EntityRecognizer.findEntity(personality_entities, 'mood'))
            msg_out = "Couldn’t be better! How can i help you?";

        else if (builder.EntityRecognizer.findEntity(personality_entities, 'positive')) {
            msg_out = 'Thank you ' + session.message.user.name + '! Is there anything else I can help you with';

            var entity_lookup_time_greeting = builder.EntityRecognizer.findEntity(personality_entities, 'builtin.datetimeV2.timerange');
            entity_lookup_time_greeting = entity_lookup_time_greeting || builder.EntityRecognizer.findEntity(personality_entities, 'help')

            if (entity_lookup_time_greeting) {
                var entityname = entity_lookup_time_greeting.entity;

                var isTimeGreeting = (entityname === 'morning' || entityname === 'afternoon' || entityname === 'evening');
                var isDayGreeting = (entityname === 'day');

                if (isTimeGreeting) {
                    msg_out = 'Good ' + entityname + ' ' + session.message.user.name + '! How can I help you?';
                }
                else if (isDayGreeting) {
                    msg_out = 'Good day to you too mate! How can I help you?';
                }
            }
        }
        else if (entity_lookup_help) {
            msg_out = "I can answer questions related to your development as a people manager, or the development of your team.  For more information on your Career at Mercer, please visit the [Me and My Career page](http://sites.mercer.com/sites/HR/default.aspx) on MercerLink.";

            var isHiGreeting = (entity_lookup_help.entity === 'hi');
            var isHeyGreeting = (entity_lookup_help.entity === 'hey' || entity_lookup_help.entity === 'hey there' || entity_lookup_help.entity === 'heythere');
            var isHelloGreeting = (entity_lookup_help.entity === 'hello');
            var isHulloGreeting = (entity_lookup_help.entity === 'hullo');
            var isThanksGreeting = (entity_lookup_help.resolution.values[0] === 'thanks');
            var isDayGreeting = (entity_lookup_help.entity === 'day');
            var isTimeQuery = (entity_lookup_help.entity === 'time');

            if (isHiGreeting) {
                msg_out = 'Hi ' + session.message.user.name + '! How can I help you?';
            }
            else if (isHelloGreeting) {
                msg_out = 'Hello ' + session.message.user.name + '! How can I help you?';
            }
            else if (isHulloGreeting) {
                msg_out = 'Hullo ' + session.message.user.name + '! How can I help you?';
            }
            else if (isHeyGreeting) {
                msg_out = 'Hey ' + session.message.user.name + '! How can I help you?';
            }
            else if (isThanksGreeting) {
                msg_out = 'You\'re welcome ' + session.message.user.name;
            }
            else if (isDayGreeting) {
                msg_out = 'Good day to too you mate! How can I help you?';
            }
            else if (isTimeQuery) {
                msg_out = 'Check the clock on the bottom right hand side of your screen.';
            }
        }
        else if (builder.EntityRecognizer.findEntity(personality_entities, 'situation'))
            msg_out = "Couldn’t be better! How can i help you?";

        else if (builder.EntityRecognizer.findEntity(personality_entities, 'about'))
            msg_out = "I'm an AI support specialist, also known as a bot. I'm a machine-programmed to assist with your questions. If you ask me simple questions or type in a few key words, I will do my best to answer you. The more questions you ask me, the more I learn!  Right now I can help you with your development as a people manager, or the development of your team.";
        else if (builder.EntityRecognizer.findEntity(personality_entities, 'self'))
            msg_out = 'Hey ' + session.message.user.name + '! How can I help you?';
        else {
            //common.captureIntentMessage(session, args.intent, msg_out + ' ' + msg_out_1);
            //common.captureFeedback(session, { type: "No_MatchingResponse_Found", feedback: "No Matching Response found" });
            sendMessage = false;
            session.beginDialog('none');
        }


        var card = JSON.parse(common.stringInject(JSON.stringify(config.get("standard_card_2")), [msg_out, msg_out_1]));
        if (sendMessage) {
            common.captureIntentMessage(session, args.intent, msg_out + ' ' + msg_out_1);
            var msg = new builder.Message(session)
                .speak(msg_out + ' ' + msg_out_1)
                .inputHint(builder.InputHint.acceptingInput)
                .addAttachment({
                contentType: "application/vnd.microsoft.card.adaptive",
                content: card
            });
            session.send(msg).endDialog();
            sendMessage = true;
        }
    }

];