var builder = require('botbuilder');
var config = require('../config');
var common = require('../lib/common');

module.exports = [
    function (session, args, next) {
        var msg_out, msg_out_1 = "";
        var sendMessage = true;
        var promotion = builder.EntityRecognizer.findEntity(args.intent.entities, 'promotion');
        var self = builder.EntityRecognizer.findEntity(args.intent.entities, 'self');

        msg_out = common.getIntentNotFoundMessage(session);

        if (promotion && self) {
            msg_out = common.getResponseWithGreetings(session) + "I suggest you set up time with your people manager and have a discussion about your career development and what you need to do to get promoted. Prepare for your discussion--it's not something to take lightly. Use the resources on the Me and My Career page, including the career development plan [template](http://sites.mercer.com/sites/HR/SitePages/Careers.aspx)";
            msg_out_1 = "[This](http://sites.mercer.com/sites/HR/Careers/Guide-to-Talking-with-Your-Manager.pdf) guide to talking to your manager is also a good resource.";
        }
        else if (promotion) {
            msg_out = common.getResponseWithGreetings(session) + "Promotion is dependent on a variety of things, such as your knowledge, skills, behaviours and ability to take on further responsibility. Also - the role needs to be available.  There isn't one way to get promoted. I suggest you set up time with your people manager and have a discussion about your career development and what you need to do to get promoted. Prepare for your discussion--it's not something to take lightly. Use the resources on the Me and My Career page, including the career development plan [template](http://sites.mercer.com/sites/HR/SitePages/Careers.aspx)";
            msg_out_1 = "[This](http://sites.mercer.com/sites/HR/Careers/Guide-to-Talking-with-Your-Manager.pdf) guide to talking to your manager is also a good resource.";
        }
        else {
            //common.captureIntentMessage(session, args.intent, msg_out);
            //common.captureFeedback(session, { type: "No_MatchingResponse_Found", feedback: "No Matching Response found" });
            sendMessage = false;
            session.beginDialog('none');
        }

        

        var card = JSON.parse(common.stringInject(JSON.stringify(config.get("standard_card_2")), [msg_out, msg_out_1]));
        if (sendMessage) {
            common.captureIntentMessage(session, args.intent, msg_out + ' ' + msg_out_1);
            session.endDialog(new builder.Message(session).addAttachment({
                contentType: "application/vnd.microsoft.card.adaptive",
                content: card
            }));
        }
        sendMessage = true;
    }
]