var builder = require('botbuilder');
var config = require('../config');
var common = require('../lib/common');
module.exports = [

    function (session, args, next) {
        var msg_out = common.getIntentNotFoundMessage(session);
        var msg_out_1 = "";
        var sendMessage = true;

        if (builder.EntityRecognizer.findEntity(args.intent.entities, 'competency'))
            msg_out = config.get("dialog_competency");
        else {
            //common.captureIntentMessage(session, args.intent, msg_out);
            //common.captureFeedback(session, { type: "No_MatchingResponse_Found", feedback: "No Matching Response found" });
            sendMessage = false;
            session.beginDialog('none');
        }

        
        // Defining Message template
        var card = JSON.parse(common.stringInject(JSON.stringify(config.get("standard_card_2")), [msg_out, msg_out_1]));
        if (sendMessage) {
            common.captureIntentMessage(session, args.intent, msg_out + ' ' + msg_out_1);
            session.endDialog(new builder.Message(session).addAttachment({
                contentType: "application/vnd.microsoft.card.adaptive",
                content: card
            }));
        }
        sendMessage = true;

    }

];