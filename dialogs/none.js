var builder = require('botbuilder');
var config = require('../config');
var common = require('../lib/common');
var regexp = require('node-regexp');

var msg_out = "", msg_out_1 = "";

module.exports = [
    function (session, next) {
        msg_out = common.getIntentNotFoundMessage(session);
        var captureIntent = true;
        //console.log("Inside None");
        const createEvent = (eventName, value, address) => {
            var msg = new builder.Message().address(address);
            msg.data.type = "event";
            msg.data.name = eventName;
            msg.data.value = value;
            return msg;
        }
        var clearchat = regexp()
            .either('clear the chat', 'clear chat', 'clearchat', 'remove chat')
            .ignoreCase()
            .toRegExp()
        
        var about = regexp()
            .either('about yourself', 'explain yourself')
            .ignoreCase()
            .toRegExp()

        var emailchat = regexp()
            .either('email the chat', 'email chat', 'emailchat', 'mail chat', 'mailchat')
            .ignoreCase()
            .toRegExp()

        var delegation = regexp()
            .either('delegate', 'delegates', 'delegated', 'delegation', 'delegating', 'deputing', 'depute', 'deputation', 'delegating')
            .ignoreCase()
            .toRegExp()

        var coach = regexp()
            .either('coach', 'coaching', 'mentor', 'mentoring')
            .ignoreCase()
            .toRegExp()

        var goalsetting = regexp()
            .either('goal setting', 'goalsetting', 'goal', 'target', 'goals', 'targets', 'targetsetting', 'target setting', 'goalsettings', 'targetsettings', 'target settings', 'goal settings')
            .ignoreCase()
            .toRegExp()

        var advocate = regexp()
            .either('advocacy', 'advocate', 'prepare myself', 'talk to', 'handle', 'advocating')
            .ignoreCase()
            .toRegExp()

        var appraisal_321 = regexp()
            .either('3-2-1', '321', '3 2 1')
            .ignoreCase()
            .toRegExp()

        var assertive = regexp()
            .either('assertive', 'assertiveness', 'assert', 'assertion', 'assertivity', 'talkative', 'confront', 'confrontation', 'self assured', 'firm', 'assertively', 'self advocacy', 'self-advocacy')
            .ignoreCase()
            .toRegExp()

        var appraisal = regexp()
            .either('appraisal', 'pfs', 'partnering for success', 'success partnering', 'partnering success')
            .ignoreCase()
            .toRegExp()

        var workshop = regexp()
            .either('workshop', 'workshops', 'class', 'classes')
            .ignoreCase()
            .toRegExp()

        if(clearchat.test(session.message.text)){
            var reply = createEvent("clearchat", session.message.text, session.message.address);
            session.privateConversationData.conversation = "";
            session.privateConversationData.intent_query = "";
            session.privateConversationData.intent_message = "";
            captureIntent = false;
            session.endDialog(reply);
        }else if(emailchat.test(session.message.text)){
            var reply = createEvent("emailchat", session.privateConversationData.conversation, session.message.address);
            captureIntent = false;
            session.endDialog(reply);
        }
        else if(about.test(session.message.text)){
            msg_out = common.getResponseWithGreetings(session) + config.get("dialog_personality_about");
        }
        else if (delegation.test(session.message.text)) {
            msg_out = common.getResponseWithGreetings(session) + config.get("dialog_training_delegation");
        }
        else if (workshop.test(session.message.text)) {
            msg_out = config.get("dialog_training_workshop");
        }
        else if (assertive.test(session.message.text)) {
            msg_out = config.get("dialog_training_assertiveness");
        }
        else if (goalsetting.test(session.message.text)) {
            msg_out = common.getResponseWithGreetings(session) + config.get("dialog_training_goalsetting");
        }
        else if (advocate.test(session.message.text)) {
            msg_out = common.getResponseWithGreetings(session) + config.get("dialog_training_advocacy");
        }
        else if (appraisal_321.test(session.message.text)) {
            msg_out = common.getResponseWithGreetings(session) + config.get("dialog_information_321");
        }
        else if (appraisal.test(session.message.text)) {
            msg_out = common.getResponseWithGreetings(session) + config.get("dialog_information_appraisal");
        }
        else if (coach.test(session.message.text)) {
            msg_out = common.getResponseWithGreetings(session) + config.get("dialog_training_coach");
        }
        else {
            common.captureIntentMessage(session, { intent: "none", score: "0" }, msg_out);
            common.captureFeedback(session, { type: "No_Intent_Found", feedback: "No intent matched" });
        }
        if(captureIntent){
            common.captureIntentMessage(session, { intent: "none", score: "0" }, msg_out + ' ' + msg_out_1);
        }

        var card = JSON.parse(common.stringInject(JSON.stringify(config.get("standard_card_2")), [msg_out, msg_out_1]));
        session.endDialog(new builder.Message(session).addAttachment({
            contentType: "application/vnd.microsoft.card.adaptive",
            content: card
        }));
    }
]