var path = require('path');
var util = require('util');
var express = require('express');
var builder = require('botbuilder');
// var GraphDialog = require('bot-graph-dialog');
var config = require('./config');
var common = require('./lib/common');
var fs = require('fs');
var cors = require('cors');

var port = process.env.PORT || 3978;
var app = express();

app.use(cors());

var microsoft_app_id = '';
var microsoft_app_password = '';

var microsoft_app_id = config.get('MICROSOFT_APP_ID');
var microsoft_app_password = config.get('MICROSOFT_APP_PASSWORD');

var user = '';

var connector = new builder.ChatConnector({
  appId: microsoft_app_id,
  appPassword: microsoft_app_password
});

var bot = new builder.UniversalBot(connector, function (session) {
  if (session.message && session.message.value) {
    // A Card's Submit Action obj was received
    //console.log(session.message.value);
    common.processSubmitAction(session, session.message.value);
    return;
  }
  else {
    session.beginDialog('none');
  }
});


//const LuisModelUrl = 'https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/27e3e7e2-28bd-4307-a054-f9b6336d21ef?subscription-key=1cf2a163d40c4214a2e2968c7c53d93f&staging=true&spellCheck=true&verbose=true&timezoneOffset=0&q=';
const LuisModelUrl =   "https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/27e3e7e2-28bd-4307-a054-f9b6336d21ef?subscription-key=6321412b1a2e4f3a84d3b1e7515d20db&staging=true&spellCheck=true&bing-spell-check-subscription-key=24baa79de1b549029bfb68a1e3bdfea7&verbose=true&timezoneOffset=0&q=";

var recognizer = new builder.LuisRecognizer(LuisModelUrl);
bot.recognizer(recognizer);
var intents = new builder.IntentDialog();

bot.dialog('personality', require('./dialogs/personality')).triggerAction({ matches: 'personality' });
bot.dialog('competancy', require('./dialogs/competancy')).triggerAction({ matches: 'competancy' });
bot.dialog('training', require('./dialogs/training')).triggerAction({ matches: 'training' });
bot.dialog('expansion', require('./dialogs/expansion')).triggerAction({ matches: 'expansion' });
bot.dialog('feedback', require('./dialogs/feedback')).triggerAction({ matches: 'feedback' });
bot.dialog('promotion', require('./dialogs/promotion')).triggerAction({ matches: 'promotion' });
bot.dialog('information', require('./dialogs/information')).triggerAction({ matches: 'information' });
bot.dialog('getCareerContext', require('./dialogs/getCareerContext'));
bot.dialog('getGrowthContext', require('./dialogs/getGrowthContext'));
bot.dialog('getpmContext', require('./dialogs/getpmContext'));
bot.dialog('none', require('./dialogs/none'));

var scenariosPath = path.join(__dirname, 'bot', 'scenarios');
var handlersPath = path.join(__dirname, 'bot', 'handlers');

/*bot.on("event", function (event) {
  console.log("event has been received");
  var msg = new builder.Message().address(event.address);
    msg.textLocale("en-us");
    if (event.name === "buttonClicked") {
        msg.text("Hi " + event.value.name);
        bot.send(msg);
    }
   bot.begin(event.address, 'none');
});
bot.on('message', function(session) {
       console.log(" hello user: " + session.message.text);
});*/
bot.on('conversationUpdate', function (activity) {
  // when user joins conversation, send instructions
  if (activity.membersAdded) {
    activity.membersAdded.forEach(function (identity) {
      if (identity.id === activity.address.bot.id) {
        bot.loadSession(activity.address, function (err, session) {
          //console.log(session.message);
          //console.log(activity);
          user = session.message.user.name;
          session.privateConversationData.conversation = "";
          //var card = JSON.parse(common.stringInject(JSON.stringify(config.get("message_card")), ["Hi! I am AIMEE, your AI support specialist. What can I help you with today?"])); // string.format(config.get("welcome_card"), session.message.user.name);
          var card = JSON.parse(common.stringInject(JSON.stringify(config.get("message_card")), ["Hi" + (user ? " " + user : "") + "! I am AIMEE (because **Artificial Intelligence Makes Everything Easier**), your AI support specialist. I can help answer your questions about your development as a people manager, or the development of your team.  What can I help you with today?"])); // string.format(config.get("welcome_card"), session.message.user.name);
          session.send(new builder.Message(session).addAttachment({
            contentType: "application/vnd.microsoft.card.adaptive",
            content: card
          }));

        });
      }
    });

  }
});

// dynamically load dialogs from external datasource
// create a GraphDialog for each and bind it to the intents object
/*process.nextTick(async () => {

  var dialogs = await loadDialogs();

  dialogs.forEach(async dialog => {
    console.log(`loading scenario: ${dialog.scenario} for regex: ${dialog.regex}`);

    var re = new RegExp(dialog.regex, 'i');
    intents.matches(re, [
      function (session) {
        session.beginDialog(dialog.path, {});
      }
    ]);

    try {
      var graphDialog = await GraphDialog.create({
        bot,
        scenario: dialog.scenario,
        loadScenario,
        loadHandler,
        customTypeHandlers: getCustomTypeHandlers()
      });
    }
    catch (err) {
      console.error(`error loading dialog: ${err.message}`);
    }

    bot.dialog(dialog.path, graphDialog.getDialog());

    console.log(`graph dialog loaded successfully: scenario ${dialog.scenario} for regExp: ${dialog.regex}`);

  });
});
*/


// this allows you to extend the json with more custom node types, 
// by providing your implementation to processing each custom type.
// in the end of your implemention you should call the next callbacks
// to allow the framework to continue with the dialog.
// refer to the customTypeStepDemo node in the stomachPain.json scenario for an example.
function getCustomTypeHandlers() {
  return [
    {
      name: 'myCustomType',
      execute: (session, next, data) => {
        console.log(`in custom node type handler: customTypeStepDemo, data: ${data.someData}`);
        return next();
      }
    }
  ];
}

// this is the handler for loading scenarios from external datasource
// in this implementation we're just reading it from a file
// but it can come from any external datasource like a file, db, etc.
function loadScenario(scenario) {
  return new Promise((resolve, reject) => {
    console.log('loading scenario', scenario);
    // implement loadScenario from external datasource.
    // in this example we're loading from local file
    var scenarioPath = path.join(scenariosPath, scenario + '.json');

    return fs.readFile(scenarioPath, 'utf8', (err, content) => {
      if (err) {
        console.error("error loading json: " + scenarioPath);
        return reject(err);
      }

      var scenarioObj = JSON.parse(content);

      // simulating long load period
      setTimeout(() => {
        console.log('resolving scenario', scenarioPath);
        resolve(scenarioObj);
      }, Math.random() * 3000);
    });
  });
}

// this is the handler for loading handlers from external datasource
// in this implementation we're just reading it from a file
// but it can come from any external datasource like a file, db, etc.
//
// NOTE:  handlers can also be embeded in the scenario json. See scenarios/botGames.json for an example.
function loadHandler(handler) {
  return new Promise((resolve, reject) => {
    console.log('loading handler', handler);
    // implement loadHandler from external datasource.
    // in this example we're loading from local file
    var handlerPath = path.join(handlersPath, handler);
    var handlerString = null;
    return fs.readFile(handlerPath, 'utf8', (err, content) => {
      if (err) {
        console.error("error loading handler: " + handlerPath);
        return reject(err);
      }
      // simulating long load period
      setTimeout(() => {
        console.log('resolving handler', handler);
        resolve(content);
      }, Math.random() * 3000);
    });
  });
}

// this is the handler for loading scenarios from external datasource
// in this implementation we're just reading it from the file scnearios/dialogs.json
// but it can come from any external datasource like a file, db, etc.
function loadDialogs() {
  return new Promise((resolve, reject) => {
    console.log('loading dialogs');

    var dialogsPath = path.join(scenariosPath, "dialogs.json");
    return fs.readFile(dialogsPath, 'utf8', (err, content) => {
      if (err) {
        console.error("error loading json: " + dialogsPath);
        return reject(err);
      }

      var dialogs = JSON.parse(content);

      // simulating long load period
      setTimeout(() => {
        console.log('resolving dialogs', dialogsPath);
        resolve(dialogs.dialogs);
      }, Math.random() * 3000);
    });
  });
}

app.post('/api/messages', connector.listen());

app.listen(port, () => {
  console.log('listening on port %s', port);
});
