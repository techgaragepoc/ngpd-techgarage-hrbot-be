var config = require('../config');
var builder = require('botbuilder');
var fs = require('fs');
var request = require('request');
var mailer = require("nodemailer");

// Use Smtp Protocol to send Email
var smtpTransport = mailer.createTransport({
    service: "hotmail",
    secureConnection: false, // TLS requires secureConnection to be false
    port: 587, // port for secure SMTP
    auth: {
        user: "aimee-bot@outlook.com",
        pass: "bot@mercer"
    }
});

module.exports = {
    sendMail: function (message, email, user) {
        var mail = {
            from: "AIMEE <aimee-bot@outlook.com>",
            to: email,
            subject: "AIMEE-Conversation",
            text: message,
            html: "Hi , </br></br> Hope you are doing good. Me and " + user + " were having some convesations which " + user + " wanted to share with you. The details as follows:</br></br>" + message + "</br></br>Thanks & Regards,</br>AIMEE"
        }
        var error = false;
        smtpTransport.sendMail(mail, function (error, response) {
            if (error) {
                console.log(error);
                error = true;
            } else {
                console.log("Email sent successfully");
            }
            smtpTransport.close();

        });
        if (error == false) {
            return "success";
        }
        else { return "error"; }
    },

    getIntentNotFoundMessage: function (session) {
        var user = (session && session.message && session.message.user && session.message.user.name ? " " + session.message.user.name : "");

        var responses = [
            "Sorry" + user + ", I may not have the information you need.Right now I can only answer questions related to your development as a people manager, or the development of your team, for example: giving feedback, having performance conversations, coaching, improving communication skills, delegation and more.",
            "Hmmm, I wish I could help you with that, but right now I can only answer questions related to your development as a people manager, or the development of your team, for example: giving feedback, having performance conversations, coaching, improving communication skills, delegation and more. ",
            "Great question, but, right now I can only answer questions related to your development as a people manager, or the development of your team, for example: giving feedback, having performance conversations, coaching, improving communication skills, delegation and more.",
            "Sorry" + user + ", I haven’t learnt the answer to that question yet.Right now I can help you with your development as a people manager, or the development of your direct reports.  I’m getting smarter every day though, so check back in a couple of weeks."
        ];

        //get a random index between 0 and max length
        var randomNumberBetween0andMax = Math.floor(Math.random() * responses.length);

        //return one of the responses.
        return responses[randomNumberBetween0andMax];
    },
    getResponseWithGreetings: function (session) {
        var finalGreetings = "Hi " + session.message.user.name + "! ";

        if (session == null || session.privateConversationData == null) {
            return finalGreetings;
        }

        //Set the flag once for every user session
        if (session.privateConversationData.usergreeted === undefined || session.privateConversationData.usergreeted === false) {
            session.privateConversationData.usergreeted = true;

            return finalGreetings; //Send the greeting message on first use.
        }

        //No need to send any greeting on next response
        return "";
    },
    getDayTimeSlot: function (timeOfDay) {
        var themessage = ('day');

        var dateInUse = new Date(timeOfDay);

        if (Number.isNaN(Date.parse(timeOfDay))) {
            //return themessage;
            dateInUse = new Date();
        }

        var thehours = dateInUse.getHours();

        var morning = ('morning');
        var afternoon = ('afternoon');
        var evening = ('evening');

        if (thehours >= 0 && thehours < 12) {
            themessage = morning;

        } else if (thehours >= 12 && thehours < 17) {
            themessage = afternoon;

        } else if (thehours >= 17 && thehours < 24) {
            themessage = evening;
        }

        return themessage;
    },
    captureFeedback: function (session, message) {
        var feedback_msg = {
            "feedback": message,
            "intent": session.privateConversationData.intent,
            "intent_score": session.privateConversationData.intent_score,
            "intent_query": session.privateConversationData.intent_query,
            "intent_message": session.privateConversationData.intent_message
        }
        request.post(
            'http://ngpd-techgarage-feedback-be.azurewebsites.net/api/feedback',
            {
                json: {
                    "Feedback": {
                        "Message": message.feedback || "NA",
                        "FeedbackType": message.type,
                        "Intent": session.privateConversationData.intent,
                        "IntentScore": session.privateConversationData.intent_score,
                        "Conversation": JSON.stringify(session.privateConversationData.intent_query),
                        "Sentiment": "NA",
                        "UserQuery": session.privateConversationData.intent_query.text,
                        "BotResponse": session.privateConversationData.intent_message,
                        "ApplicationId": config.get('ApplicationID'),
                        "ApplicationName": config.get("ApplicationName"),
                        "User": session.privateConversationData.intent_query.address.user.name,
                        "Source": session.privateConversationData.intent_query.address.channelId,
                        "newColumn": "NA"
                    }
                }
            },
            function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    console.log(body)
                }
            }
        );
        /*fs.appendFile('feedback.txt', JSON.stringify(feedback_msg) + "\n\n", function (err) {
            console.log(err);
        });*/
    },
    captureIntentMessage: function (session, intent, message) {
        session.privateConversationData.intent_query = session.message;
        session.privateConversationData.intent = intent.intent;
        session.privateConversationData.intent_score = intent.score;
        session.privateConversationData.intent_message = message;
        session.privateConversationData.conversation = session.privateConversationData.conversation || "";
        session.privateConversationData.conversation += session.privateConversationData.intent_query.address.user.name + ': ' + session.privateConversationData.intent_query.text + '</br>';
        session.privateConversationData.conversation += config.get('ApplicationID') + ': ' + session.privateConversationData.intent_message + '</br>';
    },
    stringInject: function (str, data) {
        if (typeof str === 'string' && (data instanceof Array)) {

            return str.replace(/({\d})/g, function (i) {
                return data[i.replace(/{/, '').replace(/}/, '')];
            });
        } else if (typeof str === 'string' && (data instanceof Object)) {

            for (let key in data) {
                return str.replace(/({([^}]+)})/g, function (i) {
                    let key = i.replace(/{/, '').replace(/}/, '');
                    if (!data[key]) {
                        return i;
                    }

                    return data[key];
                });
            }
        } else {

            return false;
        }
    },
    processSubmitAction: function (session, value) {
        var defaultErrorMessage = 'Please complete all the feedback parameters';
        var card;
        var capture_feedback = false;
        session.sendTyping();
        switch (value.type) {
            case 'feedback_positive':
                card = JSON.parse(this.stringInject(JSON.stringify(config.get("feedback_card")), ["Thanks for feedback!! Your feedback is important for my learning and improvement."]));
                capture_feedback = true;
                break;

            case 'feedback_negative':
                card = JSON.parse(this.stringInject(JSON.stringify(config.get("feedback_negative_card")), ["Sorry to know that!! Your feedback is important for my learning and improvement. Please click below button to leave your feedback."]));
                capture_feedback = true;
                break;
            case 'feedback_submit':
                card = JSON.parse(this.stringInject(JSON.stringify(config.get("feedback_card")), ["Thanks for your feedback. This will get reviewed by our experts for corrective actions"]));
                capture_feedback = true;
                break;
            case 'conversation_share':
                card = config.get("email_card");
                break;
            case 'email_submit':
                if (value.email == "") {
                    card = JSON.parse(this.stringInject(JSON.stringify(config.get("feedback_card")), ["Please provide recipient email ids"]));
                } else {
                    var msg = "";
                    if (value.acceptcomplete == "true") {
                        //var emailsent = this.sendMail(session.privateConversationData.conversation, value.email, session.privateConversationData.intent_query.address.user.name);
                        msg = session.privateConversationData.conversation;
                    } else {
                        msg = session.privateConversationData.intent_query.address.user.name + ': ' + session.privateConversationData.intent_query.text + '</br>';
                        msg += config.get('ApplicationID') + ': ' + session.privateConversationData.intent_message + '</br>';
                    }
                    var emailsent = this.sendMail(msg, value.email, session.privateConversationData.intent_query.address.user.name);
                    if (emailsent == "success")
                        card = JSON.parse(this.stringInject(JSON.stringify(config.get("feedback_card")), ["Conversation details has been shared with recipients"]));
                    else
                        card = JSON.parse(this.stringInject(JSON.stringify(config.get("feedback_card")), ["Error occured while sending email. Please check if email ids are correct. Contact system administrator if problem continues."]));
                }
                break;
            default:
                // A form data was received, invalid or incomplete since the previous validation did not pass
                session.send(defaultErrorMessage);
        }
        if (capture_feedback) {
            this.captureFeedback(session, value);
        }
        session.send(new builder.Message(session).addAttachment({
            contentType: "application/vnd.microsoft.card.adaptive",
            content: card
        }));
    }

};